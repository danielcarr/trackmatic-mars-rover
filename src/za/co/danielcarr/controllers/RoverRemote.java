package za.co.danielcarr.controllers;

import za.co.danielcarr.models.Rover;
import za.co.danielcarr.models.Surface;

public class RoverRemote {
    private Rover rover;
    private Surface surface;

    public void locateRoverOnSurface(Surface surface, int x, int y, char heading) throws IndexOutOfBoundsException {
        this.surface = surface;
        locateRover(x, y, heading);
    }

    public void locateRover(int x, int y, char heading) {
        if (!surface.containsPoint(x, y)) {
            throw new IndexOutOfBoundsException("That point is not on the surface this remote operates over");
        }
        this.rover = new Rover(x, y, heading);
    }

    public void defineSurface(Surface surface) {
        this.surface = surface;
    }

    public void defineSurface(int x, int y) {
        this.surface = Surface.defineWithCoordinates(x, y);
    }

    public void sendCommands(String commandList) {
        for (char command : commandList.toCharArray()) {
            executeCommand(command);
        }
    }

    public void executeCommand(char commandChar) {
        if (rover == null) {
            throw new NullPointerException("Rover has not been located yet");
        }
        switch (commandChar) {
            case 'M':
                rover.move();
                int x = rover.xCoordinate();
                int y = rover.yCoordinate();
                if (! surface.containsPoint(x, y)) {
                    throw new IndexOutOfBoundsException("The rover has fallen off the edge of the plateau");
                }
                break;
            case 'L':
                rover.rotateCounterClockwise();
                break;
            case 'R':
                rover.rotateClockwise();
                break;
            default:
                throw new IllegalArgumentException("Only 'M', 'L', and 'R' are valid commands for this rover");
        }
    }

    public Rover getCurrentRover() {
        return rover;
    }
}

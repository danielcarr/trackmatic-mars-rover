package za.co.danielcarr.controllers;

import za.co.danielcarr.models.Rover;
import za.co.danielcarr.models.Surface;
import za.co.danielcarr.controllers.RoverRemote;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;

public class GroundControl {

    private Scanner reader;
    private Surface plateau;
    private RoverRemote remote;
    private Vector<Rover> fleet;

    public GroundControl() {
        fleet = new Vector<Rover>();
        remote = new RoverRemote();
    }

    public void mapPlateau() {
        reader = new Scanner(System.in);
        String coordinates = reader.findInLine("[0-9]+ [0-9]+");
        StringTokenizer tokenizer = new StringTokenizer(coordinates, " ", false);
        int x = Integer.parseInt(tokenizer.nextToken());
        int y = Integer.parseInt(tokenizer.nextToken());
        plateau = Surface.defineWithCoordinates(x, y);
        remote.defineSurface(plateau);
    }

    public void getNextRover() {
        reader = new Scanner(System.in);
        String locationLine = reader.findInLine("[0-9]+ [0-9]+ [NWES]");
        StringTokenizer tokenizer = new StringTokenizer(locationLine, " ", false);
        int x = Integer.parseInt(tokenizer.nextToken());
        int y = Integer.parseInt(tokenizer.nextToken());
        char h = tokenizer.nextToken().charAt(0);
        remote.locateRover(x, y, h);
        fleet.add(remote.getCurrentRover());
    }

    public void moveRover() {
        reader = new Scanner(System.in);
        String instructionLine = reader.nextLine();
        remote.sendCommands(instructionLine);
    }

    public void endInput() {
        reader.close();
    }

    public void printFleetPositions() {
        for (Rover rover : fleet) {
            System.out.println(rover.toString());
        }
    }
}

package za.co.danielcarr.models;

public enum Heading {
    NORTH, EAST, SOUTH, WEST;

    public static Heading fromChar(char h) {
        Heading returnHeading;
        switch (h) {
            case 'N':
            case 'n':
                returnHeading = NORTH;
                break;
            case 'E':
            case 'e':
                returnHeading = EAST;
                break;
            case 'W':
            case 'w':
                returnHeading = WEST;
                break;
            case 'S':
            case 's':
                returnHeading = SOUTH;
                break;
            default:
                throw new IllegalArgumentException("A heading can only be one of N, S, W, or E");
        }
        return returnHeading;
    }

    public char asChar() {
        char charRepresentation;
        switch (this) {
            case NORTH:
                charRepresentation = 'N';
                break;
            case EAST:
                charRepresentation = 'E';
                break;
            case WEST:
                charRepresentation = 'W';
                break;
            case SOUTH:
                charRepresentation = 'S';
                break;
            default:
                throw new IllegalArgumentException("Argument must be a heading enum type");
        }
        return charRepresentation;
    }

    public Heading next() {
        int nextOrdinal = (this.ordinal() + 1) % 4;
        return Heading.values()[nextOrdinal];
    }

    public Heading previous() {
        int nextOrdinal = this.ordinal() - 1;
        if (nextOrdinal == -1) {
            nextOrdinal = Heading.values().length - 1;
        }
        return Heading.values()[nextOrdinal];
    }
}

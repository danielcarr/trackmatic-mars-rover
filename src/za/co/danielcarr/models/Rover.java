package za.co.danielcarr.models;

public class Rover {
    private int x;
    private int y;
    private Heading heading;

    public Rover(int x, int y, Heading heading) {
        this.x = x;
        this.y = y;
        this.heading = heading;
    }

    public Rover(int x, int y, char heading) {
        this(x, y, Heading.fromChar(heading));
    }

    public void move() {
        move(1);
    }

    public void move(int times) {
        switch (heading) {
            case NORTH:
                y += times;
                break;
            case WEST:
                x -= times;
                break;
            case SOUTH:
                y -= times;
                break;
            case EAST:
                x += times;
                break;
            default:
                throw new IllegalStateException("The rover can only face north, south, east, or west.");
        }
    }

    public void rotateClockwise() {
        rotateClockwise(1);
    }

    public void rotateClockwise(int times) {
        for (int i = 0; i < times; ++i) {
            heading = heading.next();
        }
    }

    public void rotateCounterClockwise() {
        rotateCounterClockwise(1);
    }

    public void rotateCounterClockwise(int times) {
        for (int i = 0; i < times; ++i) {
            heading = heading.previous();
        }
    }

    public int xCoordinate() {
        return x;
    }

    public int yCoordinate() {
        return y;
    }

    public String toString() {
        return String.format("%d %d %c", x, y, heading.asChar());
    }
}

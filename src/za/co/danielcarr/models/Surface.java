package za.co.danielcarr.models;

public class Surface {
    private final int originX = 0;
    private final int originY = 0;
    private int maximumX;
    private int maximumY;

    public Surface(int x, int y) {
        this.maximumX = x;
        this.maximumY = y;
    }

    public static Surface defineWithCoordinates(int x, int y) {
        return new Surface(x, y);
    }

    public boolean containsPoint(int x, int y) {
        return x >= originX && y >= originY && x <= maximumX && y <= maximumY;
    }
}

package za.co.danielcarr;

import za.co.danielcarr.controllers.GroundControl;

public class Main {

    public static void main(String[] args) {
        System.err.print("Enter plateau width and length (separated by a space): ");
        GroundControl groundControl = new GroundControl();
        groundControl.mapPlateau();
        System.err.println("Enter rover commands as per README.");
        System.err.println("Terminate input with a blank line or any other invalid rover position.");
        boolean findMoreRovers = true;
        while (findMoreRovers) {
            try {
                groundControl.getNextRover();
                groundControl.moveRover();
            } catch (NullPointerException x) {
                findMoreRovers = false;
            } catch (IllegalArgumentException x) {
                System.err.println(x.getMessage());
                System.err.println("You can command the next rover, or enter a blank line to finish");
            }
        }
        groundControl.endInput();
        groundControl.printFleetPositions();
    }
}
